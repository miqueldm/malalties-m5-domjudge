package M5;

import java.util.Scanner;

/**
 * Aquesta classe te com a la solucio del problema del DoomJudge Creat.
 * @see <a href = "">No Page</a>
 * @version 04/02/2020
 * @author Miquel Domenech Mas
 */

public class DoomJudge {

	
	/**Metode Main 
	* @param args[] com a input del main
	*/
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int i,poblacio,personesInfectades,dies,personesmortes;
		float indexMort,indexProp;
		i=sc.nextInt();
		while (i>0)
		{
			indexMort=sc.nextFloat();
			poblacio=sc.nextInt();
			personesInfectades=sc.nextInt();
			indexProp=sc.nextFloat();
			dies=sc.nextInt();
			
			personesInfectades=CalculaPersonesInfectades(personesInfectades,dies,indexProp);
			if (personesInfectades>poblacio)personesInfectades=poblacio;
			personesmortes = CalculaPersonesMortes(personesInfectades,indexMort);
			System.out.println(CalculaR(poblacio,personesInfectades,personesmortes));
			i--;
		}

		sc.close();
	}
	
	
	/** Metode el numero de persones infectades
	 * funcio rep el numero de persones infectades actualment, l'index de propagacio i els dies que passen
	 * @param personesInfectades Integer Numero persones Infectades
	 * @param dies Integer Dies
	 * @param indexProp float Index de propagacio
	 * @return int Retorna el calcul
	 */	
	public static int CalculaPersonesInfectades (int personesInfectades, int dies, float indexProp)
	{
		for (int i = 0;i<dies;i++)
			{
				personesInfectades=(int)(personesInfectades*indexProp);
			}
		return personesInfectades;
	}
	
	
	/** Metode CalculpersonesMortes
	 * funcio rep el numero de persones infectades actualmenti l'index de mortalitat de la malaltia i calcula el nombre de morts
	 * @param personesInfectades Integer Numero persones Infectades
	 * @param indexMort float Index de mortalitat
	 * @return int Retorna el calcul
	 */	
	public static int CalculaPersonesMortes (int personesInfectades,float indexMort)
	{
		return (int)(personesInfectades*indexMort);
	}
	
	
	/** Metode calculperentatge
	 * funcio un numero i despres un percentatge, calcula el percentatge del primer numero
	 * @param num Integer Numero
	 * @param percentatge double Index del precentatge
	 * @return int Retorna el calcul
	 */	
	public static int CalculPercent (int num, double percentatge)
	{
		return (int)(num*percentatge); 
	}
	
	/* Metode CalculaResultat
	 * funcio que rep per parametre la poblacio, persones infectades i persones mortes i calcula el resultat de la malaltia
	 * @param poblacio Integer Numero
	 * @param personesInfectades int Numero persones infectades
	 * @param personesmortes int numero persones mortes
	 * @return String retorna el resultat en forma de String (output)
	 */	
	public static String CalculaR (int poblacio,int personesInfectades,int personesmortes)
	{
		if (personesInfectades<CalculPercent(poblacio,0.75))
		{
			 return ("CURA "+(personesmortes)+" "+personesInfectades);
		}
		else if (personesInfectades<CalculPercent(poblacio,0.99) && personesmortes<CalculPercent(poblacio,0.75))
		{
			return ("INFECTATS "+(personesmortes)+" "+personesInfectades);
		}
		else
		{
			return ("EXTERMINI "+(personesmortes)+" "+personesInfectades);
		}
	}

}