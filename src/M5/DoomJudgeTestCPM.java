package M5;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DoomJudgeTestCPM {

	private int num1;
	private float num2;
	private int resultat;
	
	public DoomJudgeTestCPM (int num1, float num2, int resultat) {
		this.num1 = num1;
		this.num2 = num2;
		this.resultat = resultat;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{5750616, (float)0.2 ,1150123},
			{7500000, (float)0.4,3000000},
			{2241284, (float)0.35,784449},
			{7102, (float)0.002,14},
			{21145,(float)0.01,211}
		});
		
	}
	
	
	@Test
	public void testCalculaCP() {
		 int res = DoomJudge.CalculaPersonesMortes(num1, num2);
		assertEquals(res,resultat);
	}

}
