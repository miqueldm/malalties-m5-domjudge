package M5;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DoomJudgeTestCP.class, DoomJudgeTestCPI.class, DoomJudgeTestCPM.class })
public class AllTests {

}
