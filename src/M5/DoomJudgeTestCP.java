package M5;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DoomJudgeTestCP {

	private int num1;
	private double num2;
	private int resultat;
	
	public DoomJudgeTestCP (int num1, double num2, int resultat) {
		this.num1 = num1;
		this.num2 = num2;
		this.resultat = resultat;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{100, 0.75,75},
			{10000, 0.50,5000},
			{120000, 0.30,36000},
			{7500000, 0.9,6750000},
			{50000,0.75,37500}
		});
		
	}
	
	
	@Test
	public void testCalculaCP() {
		 int res = DoomJudge.CalculPercent(num1, num2);
		assertEquals(res,resultat);
	}

}
