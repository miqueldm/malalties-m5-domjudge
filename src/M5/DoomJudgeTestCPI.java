package M5;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class DoomJudgeTestCPI {

	private int num1;
	private int num2;
	private float index;
	private int resultat;
	
	public DoomJudgeTestCPI (int num1, int num2,float index, int resultat) {
		this.num1 = num1;
		this.num2 = num2;
		this.index = index;
		this.resultat = resultat;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{150000, 20, (float)1.2,5750616},
			{10000, 68, (float)1.10,6524664},
			{120000, 60, (float)1.05,2241284},
			{1000, 100, (float)1.02,7102},
			{12000, 5, (float)1.12,21145}
		});
		
	}
	
	
	@Test
	public void testCalculaPersonesInfectades() {
		 int res = DoomJudge.CalculaPersonesInfectades(num1, num2, index);
		assertEquals(res,resultat);
	}

}
